# CarCar

Team:

- Nahom Zeleke = Services
- Jose Medina = Sales
- Nahom and Jose - Inventory

## Design

![alt text](Untitled-2024-01-09-0956.png)

## Instructions

    To enjoy all of our services, please do the following,
        Fork and clone our project into your respective directory
        Run the following commands to build and start your docker images and containers
            docker volume create beta-data
            docker-compose build
            docker-compose up

        To access the admin page
            run the following command from the respective api container in the exec to create an account
                python manage.py createsuperuser

## Service microservice

Running on- http://localhost:8080
Admin - http://localhost:8080/admin

### Technicians

    List of technicians = http://localhost:8080/api/technicians/
    Create a technicians - http://localhost:8080/api/technicians/
    Delete a technician - http://localhost:8080/api/technicians/:id/

### Appointments

    List of appointments - http://localhost:8080/api/appointments/
    Create an appointment - http://localhost:8080/api/appointments/
    Delete an appointment - http://localhost:8080/api/appointments/:id/
    Set appointment as canceled - http://localhost:8080/api/appointments/:id/cancel/
    Set appointment as finished - http://localhost:8080/api/appointments/:id/finish/

### Sample data

    Technicians
        GET
        {
            "technicians": [
                {
                    "id": 1,
                    "first_name": "John",
                    "last_name": "Doe",
                    "employee_id": "1"
                },
                {
                    "id": 2,
                    "first_name": "Jane",
                    "last_name": "Doe",
                    "employee_id": "2"
                }
            ]
        }


        POST
        {
            "id": 2,
            "first_name": "Jane",
            "last_name": "Doe",
            "employee_id": 2
        }

    Appointments

        GET
        {
            "appointments": [
                {
                    "href": "/api/appointments/8/",
                    "id": 8,
                    "date_time": "2024-03-20T15:49:00+00:00",
                    "reason": "engine",
                    "status": "false",
                    "vin": "NG5D5JUOFTL5UGR5C",
                    "customer": "Jane Doe",
                    "technician": {
                        "href": "/api/technicians/2/",
                        "id": 2,
                        "first_name": "Roman",
                        "last_name": "Pierce",
                        "employee_id": "2"
                    }
                }
            ]
        }


        POST
        {
            "id": 6,
            "date_time": "2020-01-01",
            "reason": "windows",
            "status": "True",
            "vin": "JJ1B4C1674TGC2LHM",
            "customer": "Jane Doe",
            "technician": {
                "id": 1,
                "first_name": "John",
                "last_name": "Doe",
                "employee_id": "1"
            }
        }

### Models

    Technician
        first name
        lasat name
        employee id

    Appointment
        date time
        reason
        status
        vin
        customer
        technician

    AutomobileVO
        vin
        sold



The Service Microservice consists of three models, Technician, Appointment, and AutomobileVO.  The Technician model consists of the first and last name as well as the employee id of the technician. The Automobile model consists of the vin of the automobile and a boolean value that represents whether the automobile was sold or not. The Appointment model consists of the date and time, reason, status, the vin of the automobile, the customers information and the technician assigned.The Appointment and Automobile VO are in a bounded context where the AutomobileVO using the poller connects to the Inventory Microservice to get the data and provide it to the Appointment model.

## Sales microservice

Running on - http://localhost:8090
Admin - http://localhost:8090/admin

### Salespeople

    List of salespeople - http://localhost:8090/api/salespeople/
    Create a salesperson - http://localhost:8090/api/salespeople/
    Delete a salesperson - http://localhost:8090/api/salespeople/:id/

### Customer

    List of customers - http://localhost:8090/api/customers/
    Create a customer - http://localhost:8090/api/customers/
    Delete a customer - http://localhost:8090/api/customers/:id/

### Sales

    List of sales - http://localhost:8090/api/sales/
    Create a sale - http://localhost:8090/api/sales/
    Delete a sale - http://localhost:8090/api/sales/:id

### Sample Data

    Salespeople
        GET
        {
            "get_salespeople": [
                {
                    "href": "/api/salespeople/3/",
                    "id": 3,
                    "first_name": "Bob",
                    "last_name": "Doe",
                    "employee_id": "2"
                }
            ]
        }

        POST
        {
            "first_name": "Mighty",
            "last_name": "Mouse",
            "employee_id": 8
        }

    Customer
        GET
        {
            "first_name": "Donald",
            "last_name": "Duck",
            "address": "Duluth",
            "phone_number": 5551234
        }

        POST
        {
            "first_name": "Donald",
            "last_name": "Duck",
            "address": "Duluth",
            "phone_number": "5551234"
        }

    Sale
        GET
        {
            "color": "brown",
            "year": 2024,
            "vin": "1234567890qwertyu",
            "model_id": 1
        }

        POST
        {
            "automobile": "/api/automobiles/1C3CC5FBZZZZZZ/",
            "salesperson": 3,
            "customer": 1,
            "price": 5000
        }

### Models

    Salesperson
        first name
        last name
        employee id

    Customer
        first name
        last name
        address
        phone number

    Sale
        automobile
        salesperson
        customer
        price

    AutomobileVO
        vin
        sold



The Sales Microservice consists of four models, Sale, Salesperson, Customer, and AutomobileVO. The Salesperson model contains the first name, last name , and employee id of the salesperson. The Customer model contains the first name, last name, address, and phone number of the customer. The AutomobileVO model contains the vin of the automobile and a sold boolean value that represents whether an automobile was sold or not. The Sale Model contains the automobile, the salesperson, the customer, and the price of the sale. Automobile, Salesperson, and Customer in the Sale model are foreginkeys to their respective models and each are in a bounded context with the Sale model. The AutomobileVO uses the poller(every 60 sec interval) to get the data from the Inventory Microservice and provide it to the Sale model.

## Inventory microservice

Running on - http://localhost:8100
Admin -http://localhost:8100/admin

### Manufacturers

    List of manufacturers - http://localhost:8100/api/manufacturers/
    Create a manufacturer - http://localhost:8100/api/manufacturers/
    Get a specfic manufacturer - http://localhost:8100/api/manufacturers/:id/
    Update a manufacturer - http://localhost:8100/api/manufacturers/:id/
    Delete a manufacturer - http://localhost:8100/api/manufacturers/:id/

### Vehicle Models

    List of vehicle models - http://localhost:8100/api/models/
    Create a vehicle model - http://localhost:8100/api/models/
    Get a specfic model - http://localhost:8100/api/models/:id/
    Update a model - http://localhost:8100/api/models/:id/
    Delete a model - http://localhost:8100/api/models/:id/

### Automobiles

    List of automobiles - http://localhost:8100/api/automobiles/
    Create an automobile - http://localhost:8100/api/automobiles/
    Get a specific automobile - http://localhost:8100/api/automobiles/:vin/
    Update an automobile - http://localhost:8100/api/automobiles/:vin/
    Delet an automobile - http://localhost:8100/api/automobiles/:vin/

### Sample Data

    Manufacturers
        GET
        {
            "manufacturers": [
                {
                    "href": "/api/manufacturers/1/",
                    "id": 1,
                    "name": "Tesla"
                }
            ]
        }

        POST
        {
            "name": "BMW"
        }

    Vehicle Models
        GET
        {
            "models": [
                {
                    "href": "/api/models/1/",
                    "id": 1,
                    "name": "Sebring",
                    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                    "manufacturer": {
                        "href": "/api/manufacturers/1/",
                        "id": 1,
                        "name": "Chrysler"
                    }
                }
            ]
        }

        POST
        {
            "name": "M3",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/2021_BMW_M3_Competition_Automatic_3.0_Front.jpg/420px-2021_BMW_M3_Competition_Automatic_3.0_Front.jpg",
            "manufacturer_id": 5
        }

    Automobiles
        GET
        {
            "autos": [
                {
                    "href": "/api/automobiles/1C3CC5FB2AN120174/",
                    "id": 1,
                    "color": "red",
                    "year": 2012,
                    "vin": "1C3CC5FB2AN120174",
                    "model": {
                        "href": "/api/models/1/",
                        "id": 1,
                        "name": "Sebring",
                        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                        "manufacturer": {
                            "href": "/api/manufacturers/1/",
                            "id": 1,
                            "name": "Chrysler"
                        }
                    },
                    "sold": false
                }
            ]
        }

        POST
        {
            "color": "brown",
            "year": 2024,
            "vin": "1234567890qwertyu",
            "model_id": 1
        }
