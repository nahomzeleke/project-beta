import React, {useEffect,useState} from 'react'

function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.get_salespeople)
        }
    }
    useEffect (() => {
        fetchData()
    }, [])

    return (
        <>
        <h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
            {salespeople.map(salesperson => (
                <tr key={salesperson.id}>
                  <td class="px-4">{ salesperson.employee_id }</td>
                  <td>{ salesperson.first_name }</td>
                  <td>{ salesperson.last_name }</td>
                </tr>
            ))}
            </tbody>
        </table>
        </>
    )
}
export default SalespeopleList
