import React, {useEffect,useState} from 'react'

function CustomersList() {
    const [customers, setCustomers] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.get_customers)
        }
    }
    useEffect (() => {
        fetchData()
    }, [])

    return (
        <>
        <h1>Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Customer Phone</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
            {customers.map(customer => (
                <tr key={customer.id}>
                    <td class="px-4">{ customer.phone_number }</td>
                    <td>{ customer.first_name }</td>
                    <td>{ customer.last_name }</td>
                </tr>
            ))}
            </tbody>
        </table>
        </>
    )
}
export default CustomersList
