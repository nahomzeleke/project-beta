import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom'

function SalespersonForm() {

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [employeeId, setEmployeeId] = useState("")
    const navigate = useNavigate()

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const handleSumbit = async (event) => {
        event.preventDefault()

        const data = {}

        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeId

        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'content-type': 'application/json'
            }
        }

        const response = await fetch(salespersonUrl, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()
            setFirstName("")
            setLastName("")
            setEmployeeId("")
            navigate('/salespeople')

        }
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1 className="fs-1 fw-bold font-monospace">Add a New Salesperson</h1>
              <form onSubmit = {handleSumbit}>
                <div className="form-floating mb-3">
                  <input onChange= {handleFirstNameChange} value= {firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"/>
                  <label htmlFor="first_name">First name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {handleLastNameChange} value= {lastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"/>
                  <label htmlFor="last_name">Last name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmployeeIdChange} value= {employeeId} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                  <label htmlFor="employee_id">Employee Id...</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      )
}

export default SalespersonForm
