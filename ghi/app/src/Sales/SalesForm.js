import React, {useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'

function SalesForm() {
  const [vin, setVin] = useState("")
  const [automobiles, setAutomobiles] = useState([])
  const [salesperson, setSalesperson] = useState("")
  const [salespersons, setSalespersons] = useState([])
  const [customer, setCustomer] = useState("")
  const [customers, setCustomers] = useState([])
  const [price, setPrice] = useState("")
  const navigate = useNavigate()

  const handleSetVin = (event) => {
      const value = event.target.value
      setVin(value)
  }

  const handleSetSalesperson = (event) => {
      const value = event.target.value
      setSalesperson(value)
  }

  const handleSetCustomer = (event) => {
    const value = event.target.value
    setCustomer(value)
}


  const handleSetPrice = (event) => {
      const value = event.target.value
      setPrice(value)
  }

  const handleSumbit = async (event) => {
      event.preventDefault()

      const data = {}
      data.automobile = vin
      data.salesperson = salesperson
      data.customer = customer
      data.price = price
      const salesUrl = 'http://localhost:8090/api/sales/'
      const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
              'content-type': 'application/json'
          }
      }

      const response = await fetch(salesUrl, fetchConfig)

      if (response.ok) {
          const putData = {}
          putData.sold = true
          const autombileURL = `http://localhost:8100/${data.automobile}`
            const fetchConfig = {
              method: "PUT",
              body: JSON.stringify(putData),
              headers: {
                'content-type': 'application/json'
              }
            }
          const responsePUT = await fetch(autombileURL, fetchConfig)
          if (responsePUT.ok) {
            setVin("")
            setSalesperson("")
            setCustomer("")
            setPrice("")
            navigate('/sales')
          }
          else {
            alert ("Auto vin update failed!")
          }



      }
  }
  const fetchDataAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        setAutomobiles(data.autos.filter(auto => auto.sold === false))
    }
  }

  const fetchDataSalespersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        setSalespersons(data.get_salespeople)
    }
  }

  const fetchDataCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        setCustomers(data.get_customers)
    }
  }

  useEffect (() => {
      fetchDataAutomobiles()
      fetchDataSalespersons()
      fetchDataCustomers()
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="fs-1 fw-bold font-monospace">Create a New Sales</h1>
          <form onSubmit = {handleSumbit}>
            <div className="mb-3">
              <select onChange={handleSetVin} value= {vin} required name="vin" id="vin" className="form-select">
              < option value="">Select a vin</option>
                  {automobiles.map(auto => {
                    return (
                      <option key= {auto["vin"]} value={auto["href"]}>
                        {auto["vin"]}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSetSalesperson} value= {salesperson} required name="salesperson" id="salesperson" className="form-select">
              < option value="">Select a Salesperson</option>
                  {salespersons.map(get_salespeople => {
                    return (
                      <option key= {get_salespeople["id"]} value={get_salespeople["id"]}>
                        {get_salespeople["last_name"]}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSetCustomer} value= {customer} required name="customer" id="customer" className="form-select">
              < option value="">Select a Customer</option>
                  {customers.map(get_customer => {
                    return (
                      <option key= {get_customer["id"]} value={get_customer["id"]}>
                        {get_customer["last_name"]}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleSetPrice} value= {price} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
              <label htmlFor="price">Enter price...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default SalesForm
