import React, {useEffect,useState} from 'react'
function ModelsList() {
    const [models, setModels] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }
    useEffect (() => {
        fetchData()
    }, [])
    return (
        <>
        <h1>Models</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
            {models.map(model => (
                <tr key={model.id}>
                  <td class="px-4">{ model.name }</td>
                  <td class="px-4">{ model.manufacturer.name }</td>
                  <img src={model.picture_url} className="px-4" width="300" />

                </tr>
            ))}
            </tbody>
        </table>
        </>
    )
}
export default ModelsList
