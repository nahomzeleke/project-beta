import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function AppointmentForm() {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [dateTime, setDateTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState("");
  const navigate = useNavigate();

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };
  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };
  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleSumbit = async (event) => {
    event.preventDefault();

    const data = {};

    data.vin = vin;
    data.customer = customer;
    data.date_time = dateTime;
    data.technician = technician;
    data.reason = reason;

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setVin("");
      setCustomer("");
      setDateTime("");
      setTechnician("");
      setReason("");
      navigate("/appointments");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSumbit}>
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                value={vin}
                placeholder="Vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleCustomerChange}
                value={customer}
                placeholder="Customer"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleDateTimeChange}
                value={dateTime}
                placeholder="Date Time"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time">Date-Time</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleTechnicianChange}
                value={technician}
                required
                name="technician"
                id="technician"
                className="form-select"
              >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.employee_id}>
                      {technician.first_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleReasonChange}
                value={reason}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
