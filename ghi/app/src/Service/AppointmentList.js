import React, { useEffect, useState } from "react";

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const onCancel = (id) => {
    return async () => {
      await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
        method: "put",
      });
      setAppointments(
        appointments.filter((appointment) => appointment.id !== id)
      );
    };
  };

  const onFinish = (id) => {
    return async () => {
      await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
        method: "put",
      });
      setAppointments(
        appointments.filter((appointment) => appointment.id !== id)
      );
    };
  };

  const fetchData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(
        data.appointments.filter(
          (appointment) => appointment.status === "Created"
        )
      );
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technican</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.is_vip}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
              <td>{appointment.technician.first_name}</td>
              <td>{appointment.reason}</td>
              <td>
                <button
                  onClick={onCancel(appointment.id)}
                  className="btn btn-danger"
                >
                  Cancel
                </button>
                <button
                  onClick={onFinish(appointment.id)}
                  className="btn btn-success"
                >
                  Finish
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentList;
