import React, { useEffect, useState } from "react";

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [search, setSearch] = useState("");

  const handleSearchChange = (event) => {
    const value = event.target.value;
    setSearch(value);
  };

  const fetchAppointmentData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchAppointmentData();
  }, []);

  return (
    <>
      <h1>Service History</h1>
      <div className="input-group mb-3">
        <input
          onChange={handleSearchChange}
          type="text"
          className="form-control"
          placeholder="Search by VIN..."
          aria-label="Recipient's username"
          aria-describedby="button-addon2"
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technican</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appointment) =>
              search === ""
                ? true
                : appointment.vin.toLowerCase().includes(search.toLowerCase())
            )
            .map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.is_vip}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td>{appointment.technician.first_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentList;
