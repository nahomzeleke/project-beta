import React from "react";


function Footer() {
  return (
    <div className="container">
      <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <p className="col-md-4 mb-0 text-body-secondary">
        </p>
        <div className="col-md-4 text-center">
          <p>A GET IT DONE project by Nahom Z. and Jose M. </p>
        </div>
        <ul className="nav col-md-4 justify-content-end">
        </ul>
      </footer>
    </div>
  );
}

export default Footer;