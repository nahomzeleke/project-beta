import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                CarCar
              </NavLink>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownInventory"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownInventory"
              >
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/new">
                    Add a Manufacturer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models">
                    Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models/new">
                    Add a model
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/new">
                    Add an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownServices"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownServices"
              >
                <li>
                  <NavLink className="dropdown-item" to="/technicians">
                    Technicians
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians/new">
                    Add a Technician
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments">
                    Service Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/new">
                    Create a Service Appointment
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownServices"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownServices"
              >
                <li>
                  <NavLink className="dropdown-item" to="salespeople/">
                    Salespeople
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="salespeople/new/">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="customers/">
                    Customers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="customers/new/">
                    Add a Customer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="sales/">
                    Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="sales/new/">
                    Record a Sale
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
