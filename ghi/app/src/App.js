import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianList from "./Service/TechnicianList";
import TechnicansForm from "./Service/TechnicianForm";
import AppointmentList from "./Service/AppointmentList";
import AppointmentForm from "./Service/AppointmentForm";
import AppointmentHistory from "./Service/AppointmentHistory";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import ManufacturersList from "./Inventory/ManufacturersList";
import VehicleForm from "./Inventory/VehicleForm";
import ModelsList from "./Inventory/ModelsList";
import AutomobileForm from "./Inventory/AutomobileForm";
import AutomobilesList from "./Inventory/AutomobilesList";
import SalespeopleList from "./Sales/SalespeopleList";
import SalespersonForm from "./Sales/SalespersonForm";
import CustomersList from "./Sales/CustomersList";
import CustomerForm from "./Sales/CustomerForm";
import SalesForm from "./Sales/SalesForm";
import SalesList from "./Sales/SalesList";
import Footer from "./Footer";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicansForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList/>}/>
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList/>}/>
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList/>}/>
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="salespeople/">
            <Route index element={<SalespeopleList />} />
            <Route path="new/" element={<SalespersonForm />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomersList />} />
            <Route path="new/" element={<CustomerForm />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="new/" element={<SalesForm />} />
          </Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
