from django.contrib import admin
from .models import AutomobileVO, Salesperson, Customer, Sale


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = [
        "vin",
        "sold",
        "import_href",
    ]


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "employee_id",
    ]


@admin.register(Customer)
class CustomerVOAdmin(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


@admin.register(Sale)
class SaleVOAdmin(admin.ModelAdmin):
    list_display = [
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]