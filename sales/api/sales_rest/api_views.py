from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Customer, Sale, Salesperson
from .encoder import CustomerDetailEncoder, SaleDetailEncoder, SalespersonDetailEncoder
import json


@require_http_methods(["GET", "POST"])
def api_salespersons(request):
    if request.method == "GET":
        get_salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"get_salespeople": get_salespeople},
            encoder=SalespersonDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            create_salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                {"create_salesperson": create_salesperson},
                encoder=SalespersonDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"MESSAGE": "Could not CREATE the NEW SALE!"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            get_salesperson = Salesperson.objects.get(employee_id=pk)
            return JsonResponse(
                get_salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"MESSSAGE": "Could not GET Salesperson's info."})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            delete_salesperson = Salesperson.objects.get(employee_id=pk)
            delete_salesperson.delete()
            return JsonResponse(
                {"delete_salesperson": delete_salesperson},
                encoder=SalespersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"MESSAGE": "Could not DELETE Salesperson's info."})
    else:
        try:
            content = json.loads(request.body)
            update_salesperson = Salesperson.objects.get(employee_id=pk)
            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(update_salesperson, prop, content[prop])
            update_salesperson.save()
            return JsonResponse(
                {"update_salesperson": update_salesperson},
                encoder=SalespersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"MESSAGE": "Could not UPDATE Salesperson's info."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        get_customers = Customer.objects.all()
        return JsonResponse(
            {"get_customers": get_customers},
            encoder=CustomerDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            create_customer = Customer.objects.create(**content)
            return JsonResponse(
                {"create_customer": create_customer},
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"MESSAGE": "Could not CREATE a NEW Customer!"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            get_customer = Customer.objects.get(last_name=pk)
            return JsonResponse(
                get_customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"MESSSAGE": "Could not GET Customer's info."})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            delete_customer = Customer.objects.get(last_name=pk)
            delete_customer.delete()
            return JsonResponse(
                {"delete_customer": delete_customer},
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"MESSAGE": "Could not DELETE Customer's info."})
    else:
        try:
            content = json.loads(request.body)
            update_customer = Customer.objects.get(last_name=pk)

            props = ["first_name", "last_name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(update_customer, prop, content[prop])
            update_customer.save()
            return JsonResponse(
                {"update_customer": update_customer},
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"MESSAGE": "Could not UPDATE Customer's info."})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        get_sales = Sale.objects.all()
        return JsonResponse(
            {"get_sales": get_sales},
            encoder=SaleDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"MESSAGE": "Invalid automobile!"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"MESSAGE": "Invalid salespeson!"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"MESSAGE": "Invalid customer!"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            get_sale = Sale.objects.get(id=pk)
            return JsonResponse(
                get_sale,
                encoder=SaleDetailEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"MESSSAGE": "Could not GET sale info."})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            delete_sale = Sale.objects.get(id=pk)
            delete_sale.delete()
            return JsonResponse(
                {"delete_sale": delete_sale},
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"MESSAGE": "Could not DELETE sale info."})
    else:
        try:
            content = json.loads(request.body)
            update_sale = Sale.objects.get(id=pk)

            props = ["vin", "salesperson", "customer", "price"]
            for prop in props:
                if prop in content:
                    setattr(update_sale, prop, content[prop])
            update_sale.save()
            return JsonResponse(
                {"update_sale": update_sale},
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"MESSAGE": "Could not UPDATE sale info."})
            response.status_code = 404
            return response
