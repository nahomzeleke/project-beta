from django.db import models
from django.urls import reverse


class Technician(models.Model):

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.pk})

    def __str__(self):
        return self.employee_id


class AutomobileVO(models.Model):

    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

    def __str__(self):
        return self.vin


class Appointment(models.Model):

    date_time = models.DateTimeField(null=True, blank=True)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
    is_vip = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.vin
