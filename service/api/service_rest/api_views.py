from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
from .encoders import (
    TechnicianListEncoder,
    AppointmentDetailEncoder,
)
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianListEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_technician(request, pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentDetailEncoder
        )
    else:
        content = json.loads(request.body)
        content["status"] = "Created"

        auto = AutomobileVO.objects.filter(vin=content["vin"])

        if len(auto) > 0:
            if auto[0].sold:
                content["is_vip"] = "Yes"
            else:
                content["is_vip"] = "No"
        else:
            content["is_vip"] = "No"

        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(employee_id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT", "DELETE"])
def api_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods({"PUT"})
def api_cancel_appointment(request, pk):
    content = {}
    content["status"] = "Cancel"
    Appointment.objects.filter(id=pk).update(**content)
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods({"PUT"})
def api_finished_appointment(request, pk):
    content = {}
    content["status"] = "Finished"
    Appointment.objects.filter(id=pk).update(**content)
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
